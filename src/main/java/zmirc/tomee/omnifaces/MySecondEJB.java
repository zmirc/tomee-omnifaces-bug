package zmirc.tomee.omnifaces;

import javax.ejb.Stateless;

@Stateless
public class MySecondEJB {

    public int subtract(int no1, int no2) {
	return no1 - no2;
    }

}
