package zmirc.tomee.omnifaces;

import javax.ejb.Stateless;

@Stateless
public class MyEJB {

    public int sum(int no1, int no2) {
	return no1 + no2;
    }

}
