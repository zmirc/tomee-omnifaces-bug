package zmirc.tomee.omnifaces;

import javax.inject.Inject;
import static junit.framework.TestCase.assertNotNull;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTest {

    @Inject
    private MyEJB myEjb;

    @Deployment
    public static WebArchive createDeployment() {
	return ShrinkWrap.create(WebArchive.class)
		.addClass(MyEJB.class)
		.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void test() {
	assertNotNull(myEjb);
    }

}
