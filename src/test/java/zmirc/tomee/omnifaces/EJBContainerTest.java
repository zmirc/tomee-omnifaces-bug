package zmirc.tomee.omnifaces;

import java.io.IOException;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import static junit.framework.TestCase.assertNotNull;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class EJBContainerTest {
    
    private static EJBContainer c;
    private static MyEJB myEjb;
    
    @BeforeClass
    public static void start() throws IOException, NamingException {
	c = TestUtils.getEJBContainer();
	myEjb = TestUtils.loadBean(c, MyEJB.class);
    }
    
    @AfterClass
    public static void stop() {
	c.close();
    }
    
    @Test
    public void test() {
	assertNotNull(myEjb);
    }
    
}
