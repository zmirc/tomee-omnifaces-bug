package zmirc.tomee.omnifaces;

import javax.ejb.EJB;
import junit.framework.TestCase;
import org.apache.openejb.jee.EjbJar;
import org.apache.openejb.jee.StatelessBean;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.junit.Module;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(ApplicationComposer.class)
public class AppComposerTest extends TestCase {

    @EJB
    MyEJB myEjb;
    @EJB
    MySecondEJB mySecondEjb;

    @Module
    public EjbJar beans() {
	EjbJar ejbJar = new EjbJar();
	ejbJar.addEnterpriseBean(new StatelessBean(MyEJB.class));
	ejbJar.addEnterpriseBean(new StatelessBean(MySecondEJB.class));
	return ejbJar;
    }

    @Test
    public void testSum() {
	assertNotNull(myEjb);
	assertNotNull(mySecondEjb);
    }

}
