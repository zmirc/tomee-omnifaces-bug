package zmirc.tomee.omnifaces;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import org.apache.ziplock.Archive;
import static org.apache.ziplock.JarLocation.jarLocation;

public class TestUtils {

    public static EJBContainer getEJBContainer() throws IOException {
	final File webApp = Archive.archive()
		.copyTo("WEB-INF/classes", jarLocation(MyEJB.class))
		.asDir();

	InputStream is = TestUtils.class.getClassLoader().getResourceAsStream("jndi.properties");
	Properties p = new Properties();
	p.setProperty(EJBContainer.MODULES, webApp.getAbsolutePath());
	p.load(is);
	return EJBContainer.createEJBContainer(p);
    }

    public static <T extends Object> T loadBean(EJBContainer ejbc, Class beanClass) throws NamingException {
	Object o = ejbc.getContext().lookup("java:global/" + beanClass.getSimpleName());
	return (T) beanClass.cast(o);
    }
    
}
